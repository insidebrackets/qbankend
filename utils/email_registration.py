#!/usr/bin/env python
# -*- coding: utf-8 -*-

from fastapi import (BackgroundTasks, UploadFile, 
                    File, Form, Depends, HTTPException, status)

from dotenv import dotenv_values
from pydantic import BaseModel, EmailStr
from typing import List
from fastapi_mail import FastMail, MessageSchema,ConnectionConfig
import jwt
from models.users import Users  
import os
dirname = os.path.abspath(os.curdir)
env_path = dirname + "/.env"
config_credentials = dict(dotenv_values(env_path))

conf = ConnectionConfig(
    MAIL_USERNAME = config_credentials["EMAIL_ADDRESS"],
    MAIL_PASSWORD = config_credentials["EMAIL_PASS"],
    MAIL_FROM = config_credentials["EMAIL_ADDRESS"],
    MAIL_PORT = 587,
    MAIL_SERVER = "smtp.gmail.com",
    MAIL_TLS = True,
    MAIL_SSL = False,
    USE_CREDENTIALS = True
)


async def send_email(email, token):

    email_list = []
    email_list.append(email)
    template = f"""
        <!DOCTYPE html>
        <html>
        <head>
        </head>
        <body>
            <div style=" display: flex; align-items: center; justify-content: center; flex-direction: column;">
                <h3> Account verify </h3>
                <br>
                <p>Thanks for choosing EasyShopas, please 
                click on the link below to verify your account</p> 
                <a style="margin-top:1rem; padding: 1rem; border-radius: 0.5rem; font-size: 1rem; text-decoration: none; background: #0275d8; color: white;"
                 href="http://localhost:8000/verify/?token={token}">
                    Verify your email
                <a>
                <p style="margin-top:1rem;">If you did not register for EasyShopas, 
                please kindly ignore this email and nothing will happen. Thanks<p>
            </div>
        </body>
        </html>
    """

    message = MessageSchema(
        subject="EasyShopas Account Verification Mail",
        recipients=email_list,  # List of recipients, as many as you can pass 
        body=template,
        subtype="html"
        )

    fm = FastMail(conf)
    await fm.send_message(message) 
    
