from pydantic import BaseModel, Field
from typing import List, Optional
import uuid

import enum

class Category(str, enum.Enum):
    soft_drinks: str = "soft_drinks"
    bio_drinks: str = "bio_drinks"
    beer: str = "beer"
    cake: str = "cake"
    salte: str = "salate"
    menu: str = "menu"
    vegan: str = "vegan"
    vegetaria: str = "vegetaria"


class Products(BaseModel):
    titel: str
    price: float
    category: Category
    description: str = None
    images: List[str] = None 
    kind: str
    ingredients: List[str] = None
    deposit: float = None
    variety: List[str] = None
    user_id: int = None
    liter: str = None
    side_dish: List[str] = None
    extra: List[str] = None
    status : bool = None
    rating : int = Field(None, description="Rating between 1 and 5", ge=1, le=5)
        
    
     
