from pydantic import BaseModel
import enum
from fastapi import Query

class Role(str, enum.Enum):
    admin: str = "admin"
    personal: str = "user"

class Users(BaseModel):
    username: str
    password: str
    email: str = Query(..., regex="([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)")
    role: Role = None
    is_active: bool = None
    first_name: str = None
    last_name: str = None
    street: str = None
    street_number: int = None
    phone_number: int = None
    zipcode: int = None
    city: str = None
    state: str = None

class Email(BaseModel):
    email: str = Query(..., regex="([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)")

