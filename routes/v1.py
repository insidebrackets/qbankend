from fastapi import FastAPI, Body, Header, File, Depends, HTTPException, APIRouter, Request 
from models.users import Users
from models.author import Author
from models.products import Products, Category
from starlette.status import HTTP_201_CREATED, HTTP_401_UNAUTHORIZED
from starlette.responses import Response
from utils.debug import dprint, pprint
from utils.db_functions import db_check_user, db_post, db_select, db_update, db_post_delete
from utils.security import current_user, get_hashed_password
from typing import Dict
import os

app_v1  = APIRouter()

@app_v1.post("/login", tags=["User"])
async def get_user_validation(username:str=Body(...), password: str=Body(...)):
    result = await db_check_user(username, password)
    return {"is_valid":result}

@app_v1.get("/product/{serial}")
async def get_product(serial:str):

    #tname, sel=None, col=None, con=None, value=None, row=True):
    product = await db_select(tname ='products', sel='titel', col='uid', con='where', val=serial)
    return  product


@app_v1.get("/products")
async def get_all_products():
    # row = false fetches all row
    result = await db_select(tname='products',con='*', row=False)

    return result

@app_v1.get("/users")
async def get_all_users():
    # row = false fetches all row
    result = await db_select(tname='users',con='*', row=False)
    return result

@app_v1.get("/user", response_model_exclude=["password"] )
async def get_current_user( current_user = Depends(current_user)):
    # row = false fetches all row

    user_obj = await db_select(tname='users', sel='*', col='username', con='where', val=current_user)
    f = Category

    # returns returns the enum class as a dict
    category = [ { "name" : name } for name in dir(f) if not name.startswith('__')]

    # remove dict key
    user_obj.pop('password', None)

    # add enum list
    user_obj["enum"] = category
    return user_obj

@app_v1.post("/post", status_code=HTTP_201_CREATED)
async def post_value(body:Dict, current_user = Depends(current_user)):
    pprint("/Post")
    if current_user is not None:
        print(body)
        if 'password' in body:
            body["password"] = get_hashed_password(body["password"])
        result = await db_post(body, current_user)

        if result is True:
            return {'result': 'have been created'}
        if result == "username":
            return {'result': "username already exists"}
        if result == "email":
            return {'result': "email already exists"}
        else:
            return {'result': 'In error happend'}

@app_v1.patch("/update", status_code=HTTP_201_CREATED)
async def update_value(request: Request, body:Dict, current_user = Depends(current_user), safe: str = Header(None)):
    pprint("/update")

    if current_user is not None:

        if 'password' in body:
            # gets the user password from header because the json.stringify from fronted its
            # changing the password hash and storing the password again into the json
            user_password = request.headers.get("lowkey")
            body["password"] = user_password
            #body["password"] = get_hashed_password(user_password)
        
        result = await db_update(body, current_user)

        if result is True:
            return {'result': 'have been update'}
        elif(result == "exists"):
            return {'result': "username already exsts"}
        else:
            return {'result': 'In error happend'}

@app_v1.delete("/delete", status_code=HTTP_201_CREATED)
async def post_value(body:Dict, current_user = Depends(current_user)):
    pprint("/delete")
    if current_user is not None:
        print(body)
        if 'password' in body:
            body["password"] = get_hashed_password(body["password"])
        result = await db_post_delete(body)

        if result is True:
            return {'result': 'true'}
        else:
            return {'result': 'false'}

