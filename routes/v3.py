from fastapi import FastAPI, Body, Header, File, Depends, HTTPException, APIRouter 
from models.users import Users
from models.author import Author 
from models.book import Book
from starlette.status import HTTP_201_CREATED, HTTP_401_UNAUTHORIZED 
from starlette.responses import Response
from utils.debug import dprint, pprint

app_v3  = APIRouter()


@app_v3.get("/user")
async def get_user_validation(password:str):
    return {"query parameter": password}

# Exclude other from the json object 
# response_model_include=["author"]
@app_v3.get("/book/{isbn}", response_model=Book )
async def get_book_with_isbn(isbn):
    author_dict = {
                "name": "auther1",
                "book": ["book1", "book2"]
            }
    author1 = Author(**author_dict)
    book_dict = {
            "isbn": "sbn:",
            "name": "book1",
            "year": 2019,
            "author": author1
            }
    book1 = Book(**book_dict)
    return book1

@app_v3.get("/author/{id}/book")
async def get_authors_books(id: int, category: str, order:str="asc"):
    return {"query changable parameter": order + category + str(id)}

# name in a json body { "paul" } 
# name in a json body { "name": "" } embed=True
@app_v3.patch("/author/name")
async def patch_autor_name(name: str = Body(... ,embed=True)):
    return {"name in body": name }

@app_v3.post("/user/author")
async def post_user_and_author(users: Users, author: Author, 
        book:str = Body(... ,embed=True)):
    return {"user": users, "author": author, "book": book}

@app_v3.post("/user/photo")
async def upload_user_photo(resposes:Response,  profile_photo: bytes = File(...)):
    resposes.headers["x-file-size"] = str(len(profile_photo))
    return {"file size": len(profile_photo)}
