from fastapi import FastAPI, Depends, HTTPException, Request
from fastapi.security import OAuth2PasswordRequestForm 
from fastapi.staticfiles import StaticFiles
from fastapi.middleware.cors import CORSMiddleware
from routes.v1 import app_v1
from routes.v2 import app_v2
from routes.v3 import app_v3
from starlette.requests import Request
from starlette.responses import Response
from utils.security import authenticate_user, create_jwt_token, check_jwt_token, create_jwt_email_token, check_jwt_email_token, jwt_payload, get_hashed_password
from utils.db_functions import db_post, db_update, db_select 
from utils.mesages import MS_UNAUTHORIZED, MS_TOKEN_DESC, MS_TOKEN_SUMMARY, USER_AUTH_FAILED
from utils.email_registration import send_email
from utils.email_reset import reset_email
from models.jwt_user import JWTUser 
from models.users import Users, Email 
from starlette.status import HTTP_401_UNAUTHORIZED
from typing import Dict
from datetime import datetime
from utils.db_object import db
import logging
app = FastAPI(tittle="Smart Backend", description="Smart Backend made by Kal-lvi", version="1.0")

origins = [
    "http://localhost",
    "http://127.0.0.1",
    "http://localhost:8080",
    "http://127.0.0.1:8000",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(app_v1, prefix="/v1", dependencies=[Depends(check_jwt_token)])
app.include_router(app_v2, prefix="/v2", dependencies=[Depends(check_jwt_token)])
app.include_router(app_v3, prefix="/v3", dependencies=[Depends(check_jwt_token)])

app.mount("/static", StaticFiles(directory="img"), name="img")

log = logging.getLogger(__name__)
@app.on_event("startup")
async def connect_db():
    await db.connect()


@app.on_event("shutdown")
async def disconnect_db():
    await db.connect()


@app.post("/reset")
async def reset_password(email:Dict):
    try:
        user =  Email(**email) 
        user_dict = await db_select(tname='users', sel='*', col='email', con='where', val=user.email)

        if user_dict is not None:
            py_user_dict = Users(**user_dict)
            token = create_jwt_email_token(py_user_dict)
            print(token)
            await reset_email(py_user_dict.email, token)
            return token 
        else:
            return "email don't exists"

    except Exception as e:
        print(e)
        return "invalid email"
            
@app.get("/new")
async def new_password(token: str):
    return token

@app.post("/registration")
async def user_registration(user:Dict):
    new_user = Users(**user)
    new_user.is_active = False
    new_user.password = get_hashed_password(new_user.password)
    new_user.role = "user"
    new_user_dict = new_user.dict(exclude_unset=True)
    new_user_dict["tname"] = "users"

    result = await db_post(new_user_dict, current_user=None)

    if result is True:
        #return {'result': 'have been created'}
        print("New User!")   
        jwt_token = create_jwt_email_token(new_user)
        print(new_user.email)   
        await send_email(new_user.email, jwt_token)
        return "check your mail"
    if result == "username":
        return {'result': "username already exists"}
    if result == "email":
        return {'result': "email already exists"}
    else:
        return {'result': 'In error happend'}
    

from fastapi.responses import HTMLResponse

@app.get("/verify")
async def verify_user(token: str):
    payload = await check_jwt_email_token(token)
    if payload == "expired":
        return "Token is Expired" 
    # payload = username
    if payload:
        #db_update("")
        user_dict = await db_select(tname='users', sel='*', col='username', con='where', val=payload)
        print("go by or not ", user_dict)
        if user_dict["is_active"] == False:
            user_dict["tname"] = "users"
            user_dict.pop("username")
            user_dict["is_active"] = True 
            result = await db_update(user_dict, None)
            print("db_update: ", result)
            return "activted"
        else:
            return "user has already activted"
    else:
        return "wrong token"

@app.post("/token", description=MS_TOKEN_DESC, summary=MS_TOKEN_SUMMARY)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    jwt_user_dict = { "username": form_data.username, "password":form_data.password}
    jwt_user = JWTUser(**jwt_user_dict) 
    user = await authenticate_user(jwt_user)
    #log.info(jwt_user)    
    if user is None:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail=USER_AUTH_FAILED)
    
    jwt_token = create_jwt_token(user)
    return {"access_token": jwt_token}

@app.post("/atoken", description=MS_TOKEN_DESC, summary=MS_TOKEN_SUMMARY)
async def login_for_access_token(request:Request, form_data: OAuth2PasswordRequestForm = Depends()):
    jwt_user_dict = { "username": form_data.username, "password":form_data.password}
    jwt_user = JWTUser(**jwt_user_dict) 
    user = await authenticate_user(jwt_user)
    #log.info(jwt_user)    
    if user is None:
        raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail=USER_AUTH_FAILED)

    # Admin Ip
    client_host = request.client.host
    print("# client ")
    print(client_host)

    # if access if not admin
    if jwt_user.role != "admin":
       raise HTTPException(status_code=HTTP_401_UNAUTHORIZED, detail=USER_AUTH_FAILED)

    # return token
    jwt_token = create_jwt_token(user)
    return {"access_token": jwt_token}

@app.middleware("http")
async def middleware(request: Request, call_next):
    start_time = datetime.utcnow()


    response = await call_next(request)

    # modify respose
    execution_time = (datetime.utcnow() - start_time).microseconds
    response.headers["x-execution-time"] = str(execution_time)
    return response


